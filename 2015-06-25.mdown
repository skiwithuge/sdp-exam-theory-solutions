#System and Device Programming#
##25 June 2015 – Theory Solutions##

###1
Write the sequence of instructions that allow the bash process to interpret and execute the command 

    p1 | p2 > f1.txt

where p1 and p2 are two executable files.

*(answer by M. Ardizzone)*

    int pipeD[2];
    pipe(pipeD);
    
    if (fork() == 0) {
      close(1);         //Close stdout
      //dup(..) automatically assigns to the lowest available integer the file
      //descriptor passed. Since we closed 1 (stdout), pipeD[1] will be assigned
      //to 1
      dup(pipeD[1]);    //replacing stdout with pipe write
      close(pipeD[0]);  //Close read side, not needed
      //Close pipeD[1] because we don't need it anymore. It's on stdout now.
      close(pipeD[1]);
      execvp("p1", {"arg1","arg2"..."argN", NULL});
    }
    if (fork() == 0) {
      close(0);         //Close stdin
      //Same reasoning as before, but with stdin
      dup(pipeD[0]);    //replacing stdin with pipe read
      int fd = open("f1.txt",O_WRONLY,755);
      close(1);
      dup(fd);          //Redirect stdout on f1.txt
      close(fd);
      close(pipeD[1]);  //Close write side, not needed
      //Close pipeD[0] because we don't need it anymore. It's on stdin now.
      close(pipeD[0]);
      
      execvp("p2", {"arg1","arg2"..."argN", NULL});
    
    }
    close(pipeD[0]);
    close(pipeD[1]);
    wait(0);
    wait(0);

###3
Explain the behaviour of WaitForSingleObject and WaitForMultipleObjects in WIN32.
Are calls to the two functions blocking? What can we wait for, with the two functions? How many, and which, different synchronization schemes are possible through WFMO? Is it possible to use WFMO in order to wait for one among multiple events/objects?
What does constant WAIT_OBJECT_0 represent?

Given the following loop, where the tHandles array is an array of handles of running threads, and processThreadResult works on the result produced by a thread, explain what the loop does.

    /* wait thread completion 1 */
    for (iThrd = 0; iThrd < N; iThrd++) {
      WaitForSingleObject (tHandles[iThrd], INFINITE);
      processThreadResult (tData[iThrd]);
    }

Since the loop forces a given order in waiting for thread completion, write an alternative loop, based on WaitForMultipleObjects, where thread results are processed following an order given by thread completions.

*(answer by M. Ardizzone)*

`WaitForSingleObject` (WFSO) and `WaitForMultipleObjects` (WFMO) are two functions used for waiting the completion of events, threads, semaphore, mutexes and other objects in the windows environment.

WFSO is a blocking function while WFMO can be a blocking function if the parameter WaitAll is set to TRUE. 

If WFMO is set with WaitAll to FALSE then it will wait for any of the waited objects. In this case, when an object signals its completion, WFMO returns `WAIT_OBJECT_0 + n` where n is the index of the corresponding object in the array of handles WFMO is watching. In this way we can take a different action for each object (if needed).

The loop shown, waits indefinetely (INFINITE) for the completion of each thread (sequentially) and after that we will process the thread result through `processThreadResult(...)`.

This is an alternative implementation with WFMO:

    for(iThrd = 0; iThrd < N; iThrd++){
      retVal = WaitForMultipleObjects(N,tHandles,FALSE,INFINITE);
      processThreadResult(tData[retVal - WAIT_OBJECT_0]);
    }

###4
Explain the main features of dynamic libraries in Win32. Motivate the main advantages of dynamic libraries vs. static ones. Explain the difference between implicit and explicit linking. What kind of modification is required by a program in order to become a dynamic library (answer for both implicit and explicit linking).

*(answer by M. Ardizzone)*

**Static libraries** are all those libraries that after the linking phase is completed will be contained inside the same executable of the main program.

Pros:

  + No complications in building the executable
  + Pretty much portable among systems

Cons:

  - Big executable, so more memory needed
  - If a change is done in the library (or in the main source), everything must be relinked 

**Dynamic libraries** are all those libraries that even after the linking phase remain separated from the main executable file, typically into a DLL file.

Pros:

  + No need to put libraries and library functions inside the executable
  + Executable is smaller, so less memory used
  + A DLL can be shared by more applications
  + In case of new implementation of the library, I just need to change the DLL

Cons:

  - A bit of overhead for proper linking
  - More difficult to build the final program (main logic + DLL).

Dynamic Link Libraries (DLL) can be linked in two ways: implicitly or explicitly. Both methods will produce a DLL file, but the difference lies in when the library is actually loaded into the executable.

**Implicit linking** will create a DLL that will be put in the executable when it is loaded, i.e, when we double click on it. DLLs adopting this method just require the addition of the instruction `_declspec(dllexport)` and the main source file must contain `_declspec(dllimport)` instruction for proper DLL execution.
This is also the easiest method for making a DLL.

**Explicit linking** will create a DLL that will be put in the executable only when it's needed, on demand. Here it's up to the programmer to insert instruction in the right point of the code for loading a library (`LoadLibrary(..)`) and fetching the address of the required function contained in the DLL (`GetProcAddress(..)`).

###5
Which are the roles of files pointers and of the overlapped structures in direct file access on WIN32 systems. Briefly describe common aspects and differences. Provide a brief example of application for both of them. How can we increment by 100 bytes a file pointer in an overlapped structure? (provide an example)
Does an overlapped structure include an event? Is it automatically created? When is it signaled?

*(answer by M. Ardizzone)*

Both file pointers and overlapped structures (OVS) are used to access specific parts of the opened file, however they behave differently.

When a `ReadFile(..)` is performed and a file pointer is used, the pointer in the file is automatically advanced to the next record in the file. Moving the pointer to a particular location requires a call to `SetFilePointer(..)`.

When a `ReadFile(..)` is performed and an OVS is used, the overlapped structure will not point to the next element in the file, but it's up to the programmer to properly update the OVS and advance it to the next record. OVS structures can also be used for asynchronous I/O operations, a feature not available with simple file pointers. For this purpose, an event can be assigned to the OVS and it will be used to signall async I/O completion. The event must be manually created (with `CreateEvent(..)`).

Here is an example of file pointer usage:

    BOOL ReadRecord (HANDLE hFile, DWORD i, RECORD_T *pRec){
      DWORD bR; //Number of bytes read
      LARGE_INTEGER p;
      p.QuadPart = i*sizeof(RECORD_T);
      //Set the pointer to the i-th record
      SetFilePointer(hFile,p.LowPart,p.HighPart,FILE_BEGIN);
      return ReadFile (hFile, pRec, sizeof(RECORD_T), &bR, NULL);
    }

Here is the same example, with OVS:

    BOOL ReadRecord (HANDLE hFile, DWORD i, RECORD_T *pRec){
      DWORD bR; //Number of bytes read
      LARGE_INTEGER p;
      OVERLAPPED ov = {0,0,0,0,NULL};
      p.QuadPart = i*sizeof(RECORD_T);
      //Set the i-th position inside the OVS
      ov.Offset = p.LowPart;
      ov.OffsetHigh = p.HighPart;
      return ReadFile (hFile, pRec, sizeof(RECORD_T), &bR, &ov);
    }

Here is how to increment by 100 bytes a file pointer in an OVS:

    LARGE_INTEGER p;
    OVERLAPPED ov = {0,0,0,0,NULL};
    
    //We suppose each char is 1 byte
    p.QuadPart = sizeof(char)*100;
    
    ov.Offset = p.LowPart;
    ov.OffsetHigh = p.HighPart;
    
    //Now when we will read/write something
    ...