#System and Device Programming#
##22 July 2013 – Theory Solutions##

###1
Implement this precedence graph **with the minimum number of semaphores**. The threads are 4, and are **cyclic**. **There are 2 instances of the same threads** (number 3).

*(answer by M. Ardizzone)*

**Main:**

    init(S1,1);
    init(S2,0);
    init(S3,0);
    init(S4,-1);

**Thread 1:**

    while(1){
      W(S1);
      S(S2);
      S(S3);
      S(S3);
    }

**Thread 2:**

    while(1){
      W(S2);
      S(S4);
    }

**Thread 3 (x2):**

    while(1){
      W(S3);
      S(S4);
    }

**Thread 4:**

    while(1){
      W(S4);
      W(S4);
      W(S4);
      S(S1);
    }

Since T3 is a thread instantiated twice, code is just run once: when we wait for S3, we wait twice (but as soon as one S3 is signalled, one of the 2 instances of T3 will unlock). Likewise, when we signal S4, we do it twice, one for each instance.

T4 in the end will receive S4 three times: one coming from T2 and the other two coming from the two instances of T3.

###2
List and explain the steps, the files, and the processes involved in the Unix OS to make the login prompt appear in your screen.

List the steps, the files and the processes involved during the login to finally obtain your shell prompt.

*(answer by M. Ardizzone)*

The BIOS will be the first thing that will be run as the PC boots. It will initialize the PCI BUS and all the other known important devices in the system. Then it searches for a bootable device (such as a HDD, CD-ROM, USB) and then reads the bootloader form that device, transferring control to the corresponding operating system.

The bootloader will load the image of the kernel into RAM. Then, if the disk is bootable, the first sector (which, for historical reasons it contains the boot loader code) is loaded into low memory.

Some instructions are then run: first, the bootloader switches the processor from real mode to 32-bit protected mode, that's because this is the only way in which user programs can access memory locations above 1MB physical memory address space. Finally, the kernel is read directly from the disk through special I/O instructions.

Then if we are in a UNIX based operating system, the login phase is managed by the init process: first it reads `/etc/inittab` file and executes a `getty()` process (`getty()` is a UNIX system program to manage physical and virtual terminals) for each known terminal that was switched on. Getty performs then an `open()` syscall to establish connection with the required terminal and then an `exec()` call is done to run the login process, which will ask for username and password.

###3
Explain the role of filter expressions in try...except blocks. Why the following block calls the Filter function?

    __try{
      . . .
    }
    __except(Filter (GetExceptionCode ()))

Is `Filter` a system routine or a user-generated one? What is the role of `GetExceptionCode()`? Is it possible to raise a user-generated exception? (if yes, how and for what purpose, if no, why?).

*(answer by M. Ardizzone)*

`Filter` is a user-generated routine and its task is to evaluate the raised exception inside the try block and return a particular value, the value will indicate how the program should behave with that exception. The return values can be three:

  + `EXCEPTION_EXECUTE_HANDLER`: the except block is executed.
  + `EXCEPTION_CONTINUE_SEARCH`: this handler is not in charge of processing this exception, so unwind the stack and search for the next exception handler.
  + `EXCEPTION_CONTINUE_EXECUTION`: ignores the exception.

The role of `GetExceptionCode()` is to pass to `Filter` the code associated to the raised exception: this is what will be evaluated by the `Filter` function.

It is also possible to raise custom exceptions by calling `RaiseException(..)` inside the try block and the Filter function will evalue the exception code.
Usage of custom exceptions is done mainly when a program error (i.e a result of an operation that is not wrong for the system but it's against the logic of our program) occurs and we want to gracefully end the program execution (rather than haveing a program crash).

###4
Explain the support for heap management in WIN32 and motivate the advantage of using multiple heaps over libc memory management (based on malloc/calloc). Describe an example where multiple heaps can provide a better solution than a single heap.

*(answer by M. Ardizzone)*

The usage of heaps provides great advantages for memory allocation. First of all calling `HeapCreate()` among different threads, reserves a different memory area for each thread and this is a big advantage with respect to having a single heap shared among different threads because we don't need any access control (mutex if needed) on the memory area. Also, having a heap per thread allows to increase memory locality.

Heaps can also reduce memory fragmentation if for each data structure we use a heap: in this way members of the same data structure will likely be put in the same memory area.

Finally, it's possible to free all the allocated memory area just with one shot by calling `HeapDestroy()`: in this way, if further allocation inside the data structure occurred, `HeapDestroy()` will automatically release the resources. I believe it's particularly useful when the data structure contains many members that required allocation: remembering to free the memory of each of these members can be difficult and we could end up forgetting something. `HeapDestroy()` will take care of these issues.

###5
a) Describe events in the Windows system and their related system calls. Describe the 4 cases of event signal/release, related to manual/auto reset and set/pulse event conditions.

b) Write a C code implementation of a pulseEvent manualReset event (using only mutex and/or semaphores), under the condition that the number of concurrently waiting threads is <= 32.

Prototypes of mutex/semaphores functions are reported below:

    HANDLE WINAPI CreateSemaphore(LPSECURITY_ATTRIBUTES lpSemaphoreAttributes,LONG lInitialCount,LONG lMaximumCount,LPCTSTR lpName);
    HANDLE WINAPI CreateMutex(LPSECURITY_ATTRIBUTES lpMutexAttributes,BOOL bInitialOwner,LPCTSTR lpName);
    BOOL WINAPI ReleaseSemaphore( HANDLE hSemaphore, LONG lReleaseCount, LPLONG lpPreviousCount);
    BOOL WINAPI ReleaseMutex(HANDLE hMutex);

*(answer by M. Ardizzone)*

a) An **Event** is created with `CreateEvent(...)`: here we can specify if the event is created in a signalled/non-signalled state and if the state is reset automatically after it is caught.

The signal can be caught with `WaitForSingleObject(...)` and the event remains signalled if it was created with a manual-reset, so a call to `ResetEvent(...)` might be needed.

An event can be signalled by calling `SetEvent(...)` or `PulseEvent(...)` and in combination with the manual/auto reset it behaves differently:

|   |Auto Reset|Manual Reset |
|---|----------|-------------|
|`SetEvent()`|Exactly one thread is released. If none are currently waiting on the event, the next thread to wait will be released.|All currently waiting threads are released. The event remains signalled until reset by some thread.|
|`PulseEvent()`|Exactly one thread is released, but only if a thread is currently waiting on the event.|All currently waiting threads are released, and the event is then reset.|

Resources of an event are released through `CloseHandle(...)`.

b) ***Any idea for this point?***