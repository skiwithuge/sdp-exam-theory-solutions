***Disclaimer: Provided solutions are written by other students and correctness is not guaranteed (even if each answer is checked by multiple students)!***

##State of the project
As of today (July 6th, 2015) all the exam questions have been answered by at least one student.

Exams 2014-09-18 and 2015-01-21 are uploaded in PDF format under "Downloads" section. **Solution is still needed!**

Unfortunately exam 2014-09-18 is incomplete (linux questions are missing).

##What can you do?
Contribution by other students can still be useful especially for validating correctness of answers: if you wish to do so (after you've been granted write privileges), you can:

  + Add your own answer if you believe none of the available ones correctly answer the question.
  + Add your name near other students name to validate the answer: each answer is preceeded by `(answer by Student-Surname)`, you can add your surname after the last surname present.