#System and Device Programming#
##14 September 2011 – Theory Solutions##

###1
List and explain the steps, the data structures, and the processes involved in the Unix OS **to make the login windows or prompt appear in your screen.** List the steps, the files and the processes involved **during** the login to finally obtain your shell prompt.

*(answer by M. Ardizzone)*

The BIOS will be the first thing that will be run as the PC boots. It will initialize the PCI BUS and all the other known important devices in the system. Then it searches for a bootable device (such as a HDD, CD-ROM, USB) and then reads the bootloader form that device, transferring control to the corresponding operating system.

The bootloader will load the image of the kernel into RAM. Then, if the disk is bootable, the first sector (which, for historical reasons it contains the boot loader code) is loaded into low memory.

Some instructions are then run: first, the bootloader switches the processor from real mode to 32-bit protected mode, that's because this is the only way in which user programs can access memory locations above 1MB physical memory address space. Finally, the kernel is read directly from the disk through special I/O instructions.

Then if we are in a UNIX based operating system, the login phase is managed by the init process: first it reads `/etc/inittab` file and executes a `getty()` process (`getty()` is a UNIX system program to manage physical and virtual terminals) for each known terminal that was switched on. Getty performs then an `open()` syscall to establish connection with the required terminal and then an `exec()` call is done to run the login process, which will ask for username and password.

###2
What is thrashing? Which are its causes? It is possible for the system to detect and eliminate thrashing?

*(answer by M. Ardizzone)*

Thrashing is a problem affecting virtual memory and it happens when the system is constantly in a state of paging (this could be due to too many processes running or an excessive amount of memory required per process): it keeps swapping out in-memory pages and swapping in new pages from secondary memory. This affects the performance of the system in a tremendous way.

The practice of thrashing leads the CPU in an idle state. When this happens, the operating system will (falsely) believe the CPU is underused and will increase the degree of multiprogramming, introducing more processes in main memory. The new running processes will require pages, so a global replacement algorithm will start choosing page victims to swap out to make place for the new required pages, but then the old processes will required their swapped out pages again and so the cycle starts over.

Thrashing can be tackled by adopting the working-set model, which is based on the locality principle: a locality is a set of pages that are actively used together by the running process.

The working-set model uses a time window that is applied to the process' pages: as the process moves from one page to the other, the window is moved also forward and pages that are left out of the window CAN be chosen as victims by the page replacement algorithm (also, since the window moves, new pages will enter in the working set).

###3
Complete/modify the following routines for push/pop from a stack, in order to work on a multithreaded Win32 environment, where multiple threads can use a shared stack (for both push and pop operations).

    typedef struct {
    	LPDWORD data; /* array of stack items */
    	int index;
    	/* index to the stack top: for insertion */
    	int size;
    	/* size of the stack (max. number of slots available */
    	...
    } stack_t;
    ...
    BOOL push (stack_t *s, DWORD item) {
    	...
    	s->data[s->index++] = item;
    	return TRUE;
    }
    BOOL pop (stack_t *s, LPDWORD itemp) {
    	...
    	*itemp = s->data[--s->index] = item;
    	return TRUE;
    }

Synchronization data should be added to the **stack_t** struct, in order to guarantee correct access in mutual exclusion. Push and pop should be implemented in two ways:

  + blocking, so that the requesting thread will wait in case of stack not available.
  + Non blocking, with function returning TRUE upon correct completion, FALSE if stack not available.

Is deadlock possible (motivate the answer) ? If yes, provide a solution to avoid it.

*(answer by G. David)*

**Blocking**

    typedef struct {
      LPDWORD data;
      int index;
      int size;
      CRITICAL_SECTION cs;
    } stack_t;

    BOOL push (stack_t *s, DWORD item) {
      EnterCriticalSection(&s->cs);
      s->data[s->index++] = item;
      LeaveCriticalSection(&s->cs);
      return TRUE;
    }
    BOOL pop (stack_t *s, LPDWORD itemp) {
      EnterCriticalSection(&s->cs);
      *itemp = s->data[--s->index] = item;
      LeaveCriticalSection(&s->cs);
      return TRUE;
    }

**Non blocking**

    typedef struct {
      LPDWORD data;
      int index;
      int size;
      CRITICAL_SECTION cs;
    } stack_t;

    BOOL push (stack_t *s, DWORD item) {
      BOOL res = TryEnterCriticalSection(&s->cs);
      if (res == FALSE)
        return FALSE;
      s->data[s->index++] = item;
      LeaveCriticalSection(&s->cs);
      return TRUE;
    }
    BOOL pop (stack_t *s, LPDWORD itemp) {
      BOOL res = TryEnterCriticalSection(&s->cs);
      if (res == FALSE)
        return FALSE;
      *itemp = s->data[--s->index] = item;
      LeaveCriticalSection(&s->cs);
      return TRUE;
    }

*(answer by M. Ardizzone)*

**Blocking**

    typedef struct {
      LPDWORD data;
      int index;
      int size;
      HANDLE mutex;
    } stack_t;

    BOOL push (stack_t *s, DWORD item) {
      WaitForSingleObject(s->mutex,INFINITE);
      s->data[s->index++] = item;
      ReleaseMutex(s->mutex);
      return TRUE;
    }
    BOOL pop (stack_t *s, LPDWORD itemp) {
      WaitForSingleObject(s->mutex,INFINITE);
      *itemp = s->data[--s->index] = item;
      ReleaseMutex(s->mutex);
      return TRUE;
    }

**Non blocking**

    typedef struct {
      LPDWORD data;
      int index;
      int size;
      HANDLE mutex;
    } stack_t;

    BOOL push (stack_t *s, DWORD item) {
      //Wait at most 1 second
      if(WaitForSingleObject(s->mutex,1000) == WAIT_TIMEOUT)
        return FALSE;
      s->data[s->index++] = item;
      ReleaseMutex(s->mutex);
      return TRUE;
    }
    BOOL pop (stack_t *s, LPDWORD itemp) {
      if(WaitForSingleObject(s->mutex,1000) == WAIT_TIMEOUT)
        return FALSE;
      *itemp = s->data[--s->index] = item;
      ReleaseMutex(s->mutex);
      return TRUE;
    }

As is the code cannot generate deadlocks.

In the case where some control code is put to forbid pushing on a full stack or popping on an empty stack, deadlock may arise when:

  + One thread locks the mutex for popping, but the stack is empty
  + One thread locks the mutex for pushing, but the stack is full.

This is because after a thread locks the mutex for pushing/popping on a full/empty it will wait for the stack to have at least one slot free (in case of pushing) or at least one element (in case of popping). It will wait indefinitely because all the other threads cannot push/pop because mutex is locked and so deadlock will occur.

###4
Describe the main features of heaps in Win32. Why can we get benefits from using multiple heaps, with respect to standard memory management in libc? Which are the main functions for heap management in Win32?

*(answer by M. Ardizzone)*

The usage of heaps provides great advantages for memory allocation. First of all calling `HeapCreate()` among different threads, reserves a different memory area for each thread and this is a big advantage with respect to having a single heap shared among different threads because we don't need any access control (mutex if needed) on the memory area. Also, having a heap per thread allows to increase memory locality.

Heaps can also reduce memory fragmentation if for each data structure we use a heap: in this way members of the same data structure will likely be put in the same memory area.

Finally, it's possible to free all the allocated memory area just with one shot by calling `HeapDestroy()`: in this way, if further allocation inside the data structure occurred, `HeapDestroy()` will automatically release the resources. I believe it's particularly useful when the data structure contains many members that required allocation: remembering to free the memory of each of these members can be difficult and we could end up forgetting something. `HeapDestroy()` will take care of these issues.

###5
Explain the pseudo-code shown in figure, taken from concurrent ASCII-Unicode conversion of a file, using asynchronous I/O. In particular, describe how to implement concurrent updates using overlapped structures. Why is the main loop bounded **by 2\*NumRcds** ? What does **WaitForMultipleObjects** wait for ?
Is it possible for record #21 to be written on file before record #16 ? and before record #17 ? *(motivate your answers)*
![Async IO](imgs/asyncio.png)

*(answer by M. Ardizzone)*

To implement async IO with overlapped data structures, we first need to open the file with the `FILE_FLAG_OVERLAPPED` set. Second, we must create an event, in manual reset mode, and the handle of that event must be set inside the overlapped data structure. Finally, the read/write functions must take as parameter the OV structure.

The event is used because as the read/write functions are called, they return immediately and the return value provided by them is meaningless for understanding if IO is complete or not.

When an async IO is completed, the event, bind to the overlapped data structure, that is passed to the read/write function, is signalled. `WaitForMultipleObjects` will return the signalled event (in this way we can understand which IO operation is completed).

The main loop is bounded by 2\*NumRcds because we are performing 2 IO operations for each record (read and update-write).

Record 21 can be written before record 16 because they are managed by two different threads. Record 21 cannot be written before record 17 because they are managed by the same thread.
